SPICE-Library for PSPICE (Orcad)
for EPCOS AG : SIMID_0603.lib
Version 1.27
assembled on (Thu Jul 22 17:05:16 CEST 2010)

This SPICE library contains all models for 
SIMID 0603
                                                                                                      
Size 0603 (EIA)                                                                                       
and  1608 (IEC) respectively  

These linear models are suited for DC, AC and Transient-
simulations. Accurate results can be expected below
the first resonant frequency. Using these models 
outside its specification can lead to false results.

The models describe the behaviour of typical 
elements. Please keep in mind, that real components
can vary in the range given in the datasheets.

Library listing of all parts
(sorted by part numbers) :
B82496C3100G000 SIMID 0603-C standard type (10 nH +-2%)
B82496C3100J000 SIMID 0603-C standard type (10 nH +-5%)
B82496C3101G000 SIMID 0603-C standard type (100 nH +-2%)
B82496C3101J000 SIMID 0603-C standard type (100 nH +-5%)
B82496C3109A000 SIMID 0603-C standard type (1 nH +-0.3nH%)
B82496C3109Z000 SIMID 0603-C standard type (1 nH +-0.2nH%)
B82496C3120G000 SIMID 0603-C standard type (12 nH +-2%)
B82496C3120J000 SIMID 0603-C standard type (12 nH +-5%)
B82496C3121G000 SIMID 0603-C standard type (120 nH +-2%)
B82496C3121J000 SIMID 0603-C standard type (120 nH +-5%)
B82496C3129A000 SIMID 0603-C standard type (1.2 nH +-0.3nH%)
B82496C3129Z000 SIMID 0603-C standard type (1.2 nH +-0.2nH%)
B82496C3150G000 SIMID 0603-C standard type (15 nH +-2%)
B82496C3150J000 SIMID 0603-C standard type (15 nH +-5%)
B82496C3151G000 SIMID 0603-C standard type (150 nH +-2%)
B82496C3151J000 SIMID 0603-C standard type (150 nH +-5%)
B82496C3159A000 SIMID 0603-C standard type (1.5 nH +-0.3nH%)
B82496C3159Z000 SIMID 0603-C standard type (1.5 nH +-0.2nH%)
B82496C3180G000 SIMID 0603-C standard type (18 nH +-2%)
B82496C3180J000 SIMID 0603-C standard type (18 nH +-5%)
B82496C3181G000 SIMID 0603-C standard type (180 nH +-2%)
B82496C3181J000 SIMID 0603-C standard type (180 nH +-5%)
B82496C3189A000 SIMID 0603-C standard type (1.8 nH +-0.3nH%)
B82496C3189Z000 SIMID 0603-C standard type (1.8 nH +-0.2nH%)
B82496C3220G000 SIMID 0603-C standard type (22 nH +-2%)
B82496C3220J000 SIMID 0603-C standard type (22 nH +-5%)
B82496C3221G000 SIMID 0603-C standard type (220 nH +-2%)
B82496C3221J000 SIMID 0603-C standard type (220 nH +-5%)
B82496C3229A000 SIMID 0603-C standard type (2.2 nH +-0.3nH%)
B82496C3229Z000 SIMID 0603-C standard type (2.2 nH +-0.2nH%)
B82496C3270G000 SIMID 0603-C standard type (27 nH +-2%)
B82496C3270J000 SIMID 0603-C standard type (27 nH +-5%)
B82496C3279A000 SIMID 0603-C standard type (2.7 nH +-0.3nH%)
B82496C3279Z000 SIMID 0603-C standard type (2.7 nH +-0.2nH%)
B82496C3330G000 SIMID 0603-C standard type (33 nH +-2%)
B82496C3330J000 SIMID 0603-C standard type (33 nH +-5%)
B82496C3339A000 SIMID 0603-C standard type (3.3 nH +-0.3nH%)
B82496C3339Z000 SIMID 0603-C standard type (3.3 nH +-0.2nH%)
B82496C3390G000 SIMID 0603-C standard type (39 nH +-2%)
B82496C3390J000 SIMID 0603-C standard type (39 nH +-5%)
B82496C3399J000 SIMID 0603-C standard type (3.9 nH +-5%)
B82496C3399Z000 SIMID 0603-C standard type (3.9 nH +-0.2nH%)
B82496C3470G000 SIMID 0603-C standard type (47 nH +-2%)
B82496C3470J000 SIMID 0603-C standard type (47 nH +-5%)
B82496C3479J000 SIMID 0603-C standard type (4.7 nH +-5%)
B82496C3479Z000 SIMID 0603-C standard type (4.7 nH +-0.2nH%)
B82496C3560G000 SIMID 0603-C standard type (56 nH +-2%)
B82496C3560J000 SIMID 0603-C standard type (56 nH +-5%)
B82496C3569J000 SIMID 0603-C standard type (5.6 nH +-5%)
B82496C3569Z000 SIMID 0603-C standard type (5.6 nH +-0.2nH%)
B82496C3680G000 SIMID 0603-C standard type (68 nH +-2%)
B82496C3680J000 SIMID 0603-C standard type (68 nH +-5%)
B82496C3689J000 SIMID 0603-C standard type (6.8 nH +-5%)
B82496C3689Z000 SIMID 0603-C standard type (6.8 nH +-0.2nH%)
B82496C3820G000 SIMID 0603-C standard type (82 nH +-2%)
B82496C3820J000 SIMID 0603-C standard type (82 nH +-5%)
B82496C3829J000 SIMID 0603-C standard type (8.2 nH +-5%)
B82496C3829Z000 SIMID 0603-C standard type (8.2 nH +-0.2nH%)




Installation of SIMID_0603 Model Library
===============================================================

The model library of SIMID_0603 (SIMID 0603) for the 
circuit design program PSpice Schematics, version 6.1 and following
and Cadence ORCAD Capture comprises the following three files:

SIMID_0603.LIB	ASCII data file:
	        This file respresents the actual library and 
	        contains the data required for simulation. 

SIMID_0603.SLB	Symbol data file:
	        This file contains the terminal/graphics information
	        required by the graphic user interface "Schematics"

SIMID_0603.OLB	Symbol data file:
	        This file contains the terminal/graphics information
	        required by the graphic user interface "Orcad Capture"

To install the library you must copy the a.m. files into 
a ORCAD working directory (e.g. \LIB or \UserLib)


PSpice Schematics:

The library files must then be activated under PSpice. 
Proceed as follows to install the SIMID_0603 library:

1.	Start PSpice
	Install library file SIMID_0603.LIB as permanent library
		("ADD LIBRARY*") in submenu (version 9.1) 
		ANALYSIS -> LIBRARY AND INCLUDE FILES ...
		BROWSE.. (choose SIMID_0603.LIB) 
			.. ADD*
			.. OK 

	ATTENTION: Add without asterisk ("*") is a local include
		only for this actual simulation task.

2.	Install symbol data file SIMID_0603.SLB for the
	"Schematics" graphic interface in submenu
	OPTIONS -> EDITOR CONFIGURATION -> LIBRARY SETTINGS
	BROWSE.. (choose SIMID_0603.LIB) .. ADD* .. OK 


Cadence ORCAD Capture:

The library files have to be included in ORCAD Capture
Proceed as follows to install the SIMID_0603 library:

1a.	Use the delivered precompiled SIMID_0603.OLB
or	
1b.	Start ORCAD Capture 
	Translate the library file SIMID_0603.SLB
	as ORCAD library (SIMID_0603.OLB):
		FILE -> Import Design -> PSpice -> Open ...
		BROWSE.. (choose SIMID_0603.SLB) 
			.. OK  (version 10.5)
			
2.	Adding the components to your project:
	Open your project -> open a schematic
		PLACE -> PART... -> Add Library...
		(choose SIMID_0603.OLB)
		place component with OK.
		
3.	Install the library SIMID_0603.LIB for simulation:
		PSPICE -> Edit Simulation Profile -> Configuration 
		Files -> Category Library
		BROWSE.. (choose SIMID_0603.LIB)
		ADD as Global/to Design
		OK 
