#@ Encoded Library
nxp_rf_smallSignal    nxp_rf_smallSignal.rec

<?xml version="1.0" ?>
<LIBRARIES>
	<LIBRARY>
		<NAME>NXP RF SmallSignal Design Kit</NAME>
		<CATEGORY>NXP RF SmallSignal Components</CATEGORY>
                
 		<SUBLIBRARY>
			<NAME>JFETs</NAME>
			<RECORD_FILES>JFETs.rec</RECORD_FILES>
			<PLACEMENT>NOLAYOUT</PLACEMENT>
		</SUBLIBRARY> 
                                
		<SUBLIBRARY>
			<NAME>MMIC</NAME>
			<RECORD_FILES>MMIC.rec</RECORD_FILES>
			<PLACEMENT>NOLAYOUT</PLACEMENT>
		</SUBLIBRARY>
                
 		<SUBLIBRARY>
			<NAME>dual_gate_mosfets</NAME>
			<RECORD_FILES>dual_gate_mosfets.rec</RECORD_FILES>
			<PLACEMENT>NOLAYOUT</PLACEMENT>
		</SUBLIBRARY> 
                                
		<SUBLIBRARY>
			<NAME>varicap_diodes</NAME>
			<RECORD_FILES>varicap_diodes.rec</RECORD_FILES>
			<PLACEMENT>NOLAYOUT</PLACEMENT>
		</SUBLIBRARY> 
                
 		<SUBLIBRARY>
			<NAME>wideband</NAME>
			<RECORD_FILES>wideband.rec</RECORD_FILES>
			<PLACEMENT>NOLAYOUT</PLACEMENT>
		</SUBLIBRARY> 
                                                                            
 		<SUBLIBRARY>
			<NAME>power_transistors</NAME>
			<RECORD_FILES>power_transistors.rec</RECORD_FILES>
			<PLACEMENT>NOLAYOUT</PLACEMENT>
		</SUBLIBRARY> 
		
		 <SUBLIBRARY>
			<NAME>pin_diodes</NAME>
			<RECORD_FILES>pin_diodes.rec</RECORD_FILES>
			<PLACEMENT>NOLAYOUT</PLACEMENT>
		</SUBLIBRARY> 

	</LIBRARY>
</LIBRARIES>

