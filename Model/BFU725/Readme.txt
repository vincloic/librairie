Files included in this Zip file are containing de-embedded and smoothed S-parameter and noise parameters.

Filennames need to be read as follows.

Type number BFU725FN1
Voltage 1p5=1.5V=Vce
10mA=10mA=Ic
S_N means noise parameters are included.
